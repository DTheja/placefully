package com.example.ideaquest.placefully;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class InstallPhoneActivity extends AppCompatActivity {
    EditText eText;
    Button btn;
    SharedPreferences mPrefs;
    final String welcomeScreenShownPref = "askPhone";
    String phone_no = "";

    public static final String MY_PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);


        Boolean welcomeScreenShown = mPrefs.getBoolean(welcomeScreenShownPref, false);

        if (!welcomeScreenShown) {
            setContentView(R.layout.activity_install_phone);

            //added some code to add the background image...
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            ImageView   mImageView=(ImageView)findViewById(R.id.backgroundimage);
            int resID = getResources().getIdentifier("@drawable/"+"phoneinstall" , "drawable",getPackageName());
             mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(),resID, size.x,size.y));

            //
//

            eText = (EditText) findViewById(R.id.edittext);
            btn = (Button) findViewById(R.id.button);
            btn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    SharedPreferences.Editor editor = mPrefs.edit();
                    editor.putBoolean(welcomeScreenShownPref, true);
                    phone_no = eText.getText().toString();
                    editor.putString(Constants.PHONENUMBER, phone_no);
                    editor.commit(); // Very important to save the preference


                    Intent intent = new Intent(InstallPhoneActivity.this, WelcomeScreenActivity.class);
                    startActivity(intent);
                }
            });

        }
        else {
            Intent intent = new Intent(InstallPhoneActivity.this, WelcomeScreenActivity.class);
            startActivity(intent);
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}
