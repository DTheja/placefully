package com.example.ideaquest.placefully;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationMessage extends Fragment  implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    LocationRequest mLocationRequest;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLocation;
    final static int MY_PERMISSIONS_REQUEST_LOCATION = 1;
double dlat,dlon;
    double slat,slon;
    boolean Gclientconnection;
    String message, phone_no;
    Button okButton;
    String source,destination;
    public NotificationMessage() {
        // Required empty public constructor
    }

    Fragment fm;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Gclientconnection=false;
        message = getArguments().getString(Constants.FULL_ADDRESS);
        phone_no = getArguments().getString(Constants.HELP_PERSON);
        dlat=getArguments().getDouble("Latitude");
        dlon=getArguments().getDouble("Longitude");
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        destination=getAddress(dlat,dlon);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root=inflater.inflate(R.layout.fragment_notification_message, container, false);
        okButton=(Button)root.findViewById(R.id.HelpButton);
        okButton.setEnabled(false);
        Button noButton=(Button)root.findViewById(R.id.NoHelpButton);
        TextView messsagebox=(TextView)root.findViewById(R.id.notification_message);
        messsagebox.setText(phone_no+": "+message);
        fm=this;
        okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (v.isEnabled()) {
                    HashMap<String, String> postparams = new HashMap<String, String>();
                    postparams.put("self_id", phone_no);
                    postparams.put("ack_help", "on");
                    Communicate c = new Communicate(postparams);
                    c.performPostCall();
                    getActivity().getSupportFragmentManager().beginTransaction().remove(fm).commit();
                /* Probably prepare a Toast message */
                    Context context = getContext();
                    int duration = Toast.LENGTH_LONG;
                    String text = "You have been selected for this mission. Please proceed.";

                    Toast.makeText(context, text, duration).show();
                    LatLng dlt = new LatLng(dlat, dlon);
                    LatLng slt = new LatLng(slat, slon);
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr="+source+ "&daddr=" +destination));
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getContext(), "Please wait...getting your location", Toast.LENGTH_LONG).show();
                }
            }
        });

        noButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // do something when the button is clicked
                getActivity().getSupportFragmentManager().beginTransaction().remove(fm).commit();
                Context context = getContext();
                int duration = Toast.LENGTH_LONG;
                String text = "Thank you for your reply.";

                Toast.makeText(context, text, duration).show();
//               try{ Thread.sleep(3000);}catch(InterruptedException t){t.printStackTrace();}
//                Intent resultIntent = new Intent(getContext(), Notification2.class);
//                startActivity(resultIntent);
            }
        });


        return root;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission
                }
                return;
            }
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext()).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        createLocationRequest();
    }

    public void onConnected(Bundle connectionHint) {

        Gclientconnection = true;
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Toast.makeText(this, "needs the permission so that your acquaintances can see you ", Toast.LENGTH_LONG).show();

            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }

            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            slat = mLocation.getLatitude();
            slon = mLocation.getLongitude();
            source = getAddress(slat,slon);

            okButton.setEnabled(true);


    }

    String getAddress(Double lat,Double lon){
        String strAdd="";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    if(i==0)continue;
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return strAdd; }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        okButton.setEnabled(false);
        Gclientconnection = false;

    }


    @Override
    public void onConnectionSuspended(int cause) {

        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        checking();
    }

    protected void checking() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates s = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED: {
                        // Location settings are not satisfied, but this can be fixed  // by showing the user a dialog.
                        /*try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(OuterClass.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                           e.printStackTrace();
                        }*/
                        break;
                    }
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });
    }
    @Override
    public void onDestroy() {

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }
    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();


    }

}
