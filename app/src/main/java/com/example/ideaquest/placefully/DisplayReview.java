package com.example.ideaquest.placefully;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.location.Address;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class DisplayReview extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
HashMap<String,String> naming;
    String response=null;
    String myDataset[];

    LatLng [] locations;
    ArrayList<String> addr = new ArrayList<>();
    String phone_no;
    final static int  MY_PERMISSIONS_REQUEST_CONTACTS=1;
    ArrayList<ArrayList<String>> reviews = new ArrayList<ArrayList<String>>();
    public DisplayReview() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        phone_no = this.getArguments().getString(Constants.PHONENUMBER);
        naming=new HashMap<>();
       try{ mapping();}catch(SecurityException e){e.printStackTrace();}
        new Runnable(){
            public void run(){
                HashMap <String,String> postdataparams=new HashMap<>();
                double location[]= getArguments().getDoubleArray(Constants.REVIEW_POINT);
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                postdataparams.put("fetch_reviews","on");
               try{ postdataparams.put("px",Double.toString(location[0]));
                postdataparams.put("py",Double.toString(location[1]));}
               catch(NullPointerException s){s.printStackTrace();}
                postdataparams.put("self_id", phone_no);

                Communicate c=new Communicate(postdataparams);
                response=c.performPostCall();

                try {
                    JSONObject obj = new JSONObject(response);
                    JSONObject obj1 = obj.getJSONObject("result");
                    double latitude, longitude;


                    if (obj1.length() != 0) {
                        for (int i = 0; i < obj1.names().length(); i++) {
                            String key = obj1.names().getString(i);
                            JSONObject inner_obj = obj1.getJSONObject(key);
                            latitude = Double.parseDouble((inner_obj.getString("lat")));
                            longitude = Double.parseDouble((inner_obj.getString("lon")));

                            String tmp = getCompleteAddressString(latitude, longitude);

                            addr.add(tmp);

                            ArrayList<String> tmp_review = new ArrayList<String>();
                            ArrayList<String> tmp_selfid = new ArrayList<String>();
                            ArrayList<Double> tmp_time = new ArrayList<Double>();

                            JSONArray reviews_per_locn = inner_obj.getJSONArray("info");
                            if(naming!=null){
                            for (int j = 0; j < reviews_per_locn.length(); j++) {
                                JSONObject inner_inner_obj = reviews_per_locn.getJSONObject(j);
                                tmp_review.add(naming.get(inner_inner_obj.getString("self_id")) + "\n" +
                                        inner_inner_obj.getString("time") + ":\n " + inner_inner_obj.getString("review"));
                                }
                            }
                            else{
                                for (int j = 0; j < reviews_per_locn.length(); j++) {
                                    JSONObject inner_inner_obj = reviews_per_locn.getJSONObject(j);
                                    tmp_review.add(inner_inner_obj.getString("self_id") + "\n" +
                                            inner_inner_obj.getString("time") + ":\n " + inner_inner_obj.getString("review"));
                                }
                            }
                            reviews.add(tmp_review);
                        }
                    }

                }
                catch (JSONException e) {
                    Log.e("MYAPP", "unexpected JSON exception", e);
                }
                /*
                myDataset=response.arrsplit(":");
                //get the number of locations being sent;eg size
                locations=new LatLng[size];
                for(int i=0;i<size;i++) {locations[i]=new Latlng(lat ,lon);
                addr.add(i,geocoder.getFromLocation(latitude, longitude, 1).get(0));
                } */
            }}.run();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.fragment_display_review, container, false);
        mRecyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        if (addr != null) {
            mAdapter = new MyAdapter(addr.toArray(new String[0]));
//            System.out.println("....."+mAdapter+"....");
        }
        else {
//            System.out.println("As expected, we get no reviews :v");
        }
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                       // LatLng l=locations[position];
                        Bundle b=new Bundle();
                        b.putStringArrayList(Constants.REVIEW_STRING,reviews.get(position));
                        ElaborateReview er=new ElaborateReview();
                        er.setArguments(b);
                        displayFragment(er,"Reviews");
                    }
                })
        );
        return root;
    }

    private String getAddress(double latitude, double longitude) {
        StringBuilder result = new StringBuilder();
        String result1 = "";
        try {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = null;

            addresses = geocoder.getFromLocation(latitude, longitude, 1);


            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                result.append(address.getLocality()).append("\n");
                result.append(address.getCountryName());
                String addressa = address.getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String zip = addresses.get(0).getPostalCode();
                String country = addresses.get(0).getCountryName();
                result1 = addressa + ", " + city + ", " + state + ", " + zip +", " + country;

            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
        return result1;
    }
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }
    private void displayFragment(Fragment fragment, String title) {
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            // Set Toolbar Title


        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CONTACTS:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the contacts-related task you need to do.
                }
                else {
                    // permission denied, boo! Disable the functionality that depends on this permission
                }
                return;

            }
        }
    }

    public void mapping() {

        ArrayList<String> contacts = new ArrayList<>();
        ContentResolver cr = getContext(). getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        try {
                            String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                            String str = phoneNo.replaceAll("[^0-9]", "");
                            str = str.replaceAll("\\s+", "");
                            str = str.substring(str.length() - 10);

                            contacts.add(str);
                            naming.put(str, name);
                            break;
                        }
                        catch (RuntimeException e) {
                            e.printStackTrace();
                        }


                        //  Toast.makeText(NativeContentProvider.this, "Name: " + name + ", Phone No: " + phoneNo, Toast.LENGTH_SHORT).show();
                    }
                    pCur.close();
                }
            }

        }
        cur.close();
    }

}
