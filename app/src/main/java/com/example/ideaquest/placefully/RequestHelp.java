package com.example.ideaquest.placefully;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class RequestHelp extends Fragment {
    String response, phone_no;
    int duration;
    Context context;
    String text;

    public RequestHelp() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request_help, container, false);
        phone_no = this.getArguments().getString(Constants.PHONENUMBER);
        Button high = (Button)view.findViewById(R.id.button);
        Button medium = (Button)view.findViewById(R.id.button2);
        Button low = (Button)view.findViewById(R.id.button3);
        context = getContext();
        duration = Toast.LENGTH_LONG;
        text = "Hang on, help will reach you shortly.";

        high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,String> postdataparams = new HashMap<>();
                String response;

                /* TODO: Get location from arguments */

                postdataparams.put("self_id", phone_no);
                postdataparams.put("sos_call_high", "on");
                Communicate c = new Communicate(postdataparams);
                response = c.performPostCall();
                Toast.makeText(context, text, duration).show();

                /* Needn't handle the response */
            }
        });

        medium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,String> postdataparams = new HashMap<>();
                String response;
                /* TODO: Get location from arguments */
                postdataparams.put("self_id", phone_no);
                postdataparams.put("sos_call_med", "on");
                Communicate c = new Communicate(postdataparams);
                response = c.performPostCall();
                Toast.makeText(context, text, duration).show();

            }
        });

        low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String,String> postdataparams = new HashMap<>();
                String response;

//              /* TODO: Get location from arguments */
//
                postdataparams.put("self_id", phone_no);
                postdataparams.put("sos_call_low", "on");
                Communicate c = new Communicate(postdataparams);
                response = c.performPostCall();
                Toast.makeText(context, text, duration).show();

            }
        });
        return view;
    }

}
