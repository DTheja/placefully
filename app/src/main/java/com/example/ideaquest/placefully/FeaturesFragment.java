package com.example.ideaquest.placefully;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Address;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;


public class FeaturesFragment extends Fragment {
    ListView listview;
    SharedPreferences mPref;

    String phone_no;

    public FeaturesFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
        mPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        phone_no = mPref.getString(Constants.PHONENUMBER, "0000000000");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_features, container, false);
//        Display display = getActivity().getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        ImageView mImageView=(ImageView)root.findViewById(R.id.features);
//        int resID = getResources().getIdentifier("@drawable/phoneinstall" ,"drawable",root.getContext().getPackageName());
//        mImageView.setImageBitmap(decodeSampledBitmapFromResource(getResources(),resID, size.x,size.y));
        String[] features = {"Locate Friends", "Add Review", "Locate Review", "Find Help"};
        listview = (ListView) root.findViewById(R.id.features_list_view);
        listview.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, android.R.id.text1, features));
        setListners();
        Toast.makeText(getContext(),"Please put on your GPS ", Toast.LENGTH_LONG).show();
        return root;
    }



    public void setListners() {
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                // MainActivity ma = (MainActivity) getActivity();
                // Toast.makeText(getContext(), "Stop Clicking me", Toast.LENGTH_SHORT).show();
                Context context = getContext();
                int duration = Toast.LENGTH_SHORT;
                String text = "Loading, Please Wait ...";


                switch (position) {

                    case 0:{
                        Toast.makeText(context, text, duration).show();
                        MainActivityFragment fg=new MainActivityFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PHONENUMBER,phone_no);
                        fg.setArguments(bundle);
                    displayFragment(fg,"maps");

                    }
                    break;
//                    case 0: {
//                        LocateFriends lf = new LocateFriends();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Constants.PHONENUMBER,phone_no);
//                        lf.setArguments(bundle);
//                        displayFragment(lf, "Locatefriends");
//                        break;
//                    }
                    case 1: {
                        AddReview wr = new AddReview();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Constants.PHONENUMBER,phone_no);
//                        bundle.putString(ActivityConstants.CONNECTION_KEY, Chandle);
//                        sf.setArguments(bundle);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PHONENUMBER,phone_no);
                        wr.setArguments(bundle);
                        displayFragment(wr, "Addressview");
                        Toast.makeText(context, text, duration).show();
                        break;
                    }
                    case 2: {
                        LocateReview lr = new LocateReview();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Constants.PHONENUMBER,phone_no);
////                        bundle.putString(ActivityConstants.CONNECTION_KEY, Chandle);
//                        sf.setArguments(bundle);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PHONENUMBER,phone_no);
                        lr.setArguments(bundle);
                        displayFragment(lr, "Locatereview");
                        Toast.makeText(context, text, duration).show();
                        break;
                    }
                    case 3: {
                        RequestHelp rh = new RequestHelp();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(Constants.PHONENUMBER,phone_no);
//                        bundle.putString(ActivityConstants.CONNECTION_KEY, Chandle);
//                        sf.setArguments(bundle);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.PHONENUMBER,phone_no);
                        rh.setArguments(bundle);
                        displayFragment(rh, "Findhelp");

                        break;
                    }
                }
            }
        });
    }


    private void displayFragment(Fragment fragment, String title) {
        if (fragment != null) {
            FragmentManager fragmentManager =getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

            // Set Toolbar Title
            //getSupportActionBar().setTitle(title);


        }
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}

