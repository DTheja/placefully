package com.example.ideaquest.placefully;



import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class WriteReview extends Fragment {

    String phone_no;
    EditText etext;
    double arr[];

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            arr = getArguments().getDoubleArray(Constants.REVIEW_POINT);
            phone_no=getArguments().getString(Constants.PHONENUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root = inflater.inflate(R.layout.fragment_write_review, container, false);
        Button add = (Button) root.findViewById(R.id.addButton);
        etext =(EditText)root.findViewById(R.id.editText);
        add.setOnClickListener( new View.OnClickListener() {
            public void onClick(View v) {
                addreview();
            }
        });
        return root;
    }

    void addreview(){
        //TelephonyManager tMgr = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        // String mPhoneNumber = tMgr.getLine1Number();
        String review = etext.getText().toString();

        HashMap<String,String> postdataparams = new HashMap<>();
        String response;

                /* TODO: Get location from arguments */

        postdataparams.put("px",Double.toString(arr[0]));
        postdataparams.put("py",Double.toString(arr[1]));
        postdataparams.put("self_id", phone_no);
        postdataparams.put("review", review);
        postdataparams.put("give_reviews", "on");
        Communicate c = new Communicate(postdataparams);
        response = c.performPostCall();
        /* No need to proess the response */
        Context context = getContext();
        int duration = Toast.LENGTH_LONG;
        String text = "Thank you for your review.";

        Toast.makeText(context, text, duration).show();
    }
}
