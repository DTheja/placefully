package com.example.ideaquest.placefully;

import android.app.PendingIntent;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class Notification extends Fragment {


    public Notification() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Intent intent;
            intent = getActivity().getIntent();
            //String message = intent.getStringExtra(Constants.NOTIFICATIONRESPONSE);
            String self_id = intent.getStringExtra(Constants.SELF_ID);
            String latitude = intent.getStringExtra(Constants.LATITUDE);
            String longitude = intent.getStringExtra(Constants.LONGITUDE);
            Geocoder geocoder;
            Double lat, lon;
            lat = Double.parseDouble(latitude);
            lon = Double.parseDouble(longitude);

            geocoder = new Geocoder(getContext(), Locale.getDefault());
            String faddress = "";
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lon, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                faddress = knownName+"\n"+address+city+state+country;
                System.out.println(address);
            } catch (IOException e) {
                e.printStackTrace();
            }
            /* We can discuss about this.
            Intent i =new Intent();
            i.putExtra(Constants.FULL_ADDRESS,"faddress");
            // no need to create an artificial back stack.
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            getContext(),
                            0,
                            i,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            */
            Bundle b= new Bundle();
            b.putString(Constants.FULL_ADDRESS, faddress);
            b.putString(Constants.HELP_PERSON, self_id);
            NotificationMessage nm=new NotificationMessage();
            nm.setArguments(b);
            displayFragment(nm,"Notification Message");

        }
        /* TODO: Move to the fragment NotificationMessage after this, and bundle the address
        * Also, what the hell is this ^ ?*/


    }

    private void displayFragment(Fragment fragment, String title){
        if (fragment != null){
            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            // Set Toolbar Title
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }


}