package com.example.ideaquest.placefully;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.Application;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    Timer t;

    String response;
    String phone_no;
    SharedPreferences mPref;
    ArrayList<String> contacts = new ArrayList<>();
    HashMap<String,String> mapping;
    final static int  MY_PERMISSIONS_REQUEST_CONTACTS=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPref = PreferenceManager.getDefaultSharedPreferences(this);
        phone_no = mPref.getString(Constants.PHONENUMBER, "0000000000");
        mapping=new HashMap<>();
        if (findViewById(R.id.fragment_container) != null) {
            FeaturesFragment firstFragment = new FeaturesFragment();
            firstFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, firstFragment).commit();

        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_CONTACTS)) {
                //Toast.makeText(this, "needs the permission so that your acquaintances can see you ", Toast.LENGTH_LONG).show();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_CONTACTS);
            }
        }
        /* TODO: Add the contacts in a thread */
        try {
            contacts = getContacts();
        }
        catch (SecurityException e){e.printStackTrace();}

        /* Converting the HashMap of contacts to JSON for ease of use */
        JSONArray cont = new JSONArray(contacts);
        String contacts_string = cont.toString();

        HashMap<String,String> postDataParams=new HashMap<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        postDataParams.put("contacts_send", "on");
        postDataParams.put("contact_file", contacts_string);
        postDataParams.put("px", "0.0");
        postDataParams.put("py", "0.0");
        postDataParams.put("self_id", phone_no);


        Communicate c = new Communicate(postDataParams);
        response = c.performPostCall();



        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                //System.out.println("............thread is running in main Activity .............");
                /* Thread for sending online updates and receving notifications */
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                HashMap<String,String> postdataparams=new HashMap<>();

                /* TODO: Get location */  

                postdataparams.put("self_id", phone_no);
                Communicate c = new Communicate(postdataparams);
                response = c.performPostCall();
                String self_id="", lat="", lon="";
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    int sos = jsonObject.getInt("sos");
                    ;
                    //sos = 1;
                    if (sos == 1)
                    {
                        JSONObject obj = jsonObject.getJSONObject("result");
                        self_id = obj.getString("self_id");
                        lat = obj.getString("lat");
                        System.out.println("...........latitude in main activity.."+lat);

                        lon = obj.getString("lon");
                        System.out.println("..............longitude in main activity.."+lon);
                        setNotification(self_id, lat, lon,self_id);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //TODO: write the logic for handling the response from the server.
            }

        }, 0, 5000);

    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_features, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CONTACTS:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the contacts-related task you need to do.
                }
                else {
                    // permission denied, boo! Disable the functionality that depends on this permission
                }
                return;

            }
        }
    }

    public ArrayList<String> getContacts() {

        ArrayList<String> contacts = new ArrayList<>();
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        try {

                            String str = phoneNo.replaceAll("[^0-9]", "");
                            str = str.replaceAll("\\s+", "");
                            str = str.substring(str.length() - 10);

                            contacts.add(str);
                            break;
                        }
                        catch (RuntimeException e) {
                            e.printStackTrace();
                        }

                        mapping.put(phoneNo, name);
                        //  Toast.makeText(NativeContentProvider.this, "Name: " + name + ", Phone No: " + phoneNo, Toast.LENGTH_SHORT).show();
                    }
                    pCur.close();
                }
            }

        }
        cur.close();

        return  contacts;
    }
    public void setNotification(String self_id, String lat, String lon,String number)
    {

        //String name=mapping.get(number);
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle("My notification_icon")
                        .setContentText(self_id+" Needs Help !");
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Notification2.class);
        //resultIntent.putExtra(Constants.NOTIFICATIONRESPONSE, "response");
        resultIntent.putExtra("SELF_ID", self_id);
        resultIntent.putExtra("LATITUDE", lat);
        resultIntent.putExtra("LONGITUDE", lon);


        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Notification2.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);
//        PendingIntent contentIntent =
//                PendingIntent.getActivity(this, 0, new Intent(this, MyActivity.class), 0);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(123, mBuilder.build());

    }


    @Override
    protected void onDestroy() {
        t.purge();
        super.onDestroy();
    }
}
