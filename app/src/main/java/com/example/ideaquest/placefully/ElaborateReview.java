package com.example.ideaquest.placefully;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.HashMap;


public class ElaborateReview extends Fragment {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<String> myDataset;
    String response;
    public ElaborateReview() {
        // Required empty public constructor
    }

    String reviews[];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        reviews=getArguments().getStringArrayList(Constants.REVIEW_STRING).toArray(new String[0]);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.fragment_elaborate_review, container, false);
        mRecyclerView=(RecyclerView) root.findViewById(R.id.recycler_view2);
        mLayoutManager = new LinearLayoutManager(getContext());
        myDataset=getArguments().getStringArrayList(Constants.REVIEW_STRING);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(reviews);
        mRecyclerView.setAdapter(mAdapter);
        return root;
    }

}