package com.example.ideaquest.placefully;


import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivityFragment extends Fragment implements OnMapReadyCallback,GoogleMap.OnMyLocationButtonClickListener,GoogleMap.OnMapClickListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,LocationListener {

    MapView mapView;
    GoogleMap mMap;
    private boolean mPermissionDenied = false;
    boolean b;
    Timer t;


    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    LocationRequest mLocationRequest;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLocation;
    final static int MY_PERMISSIONS_REQUEST_LOCATION = 1;
    protected Boolean mRequestingLocationUpdates;
    Boolean Gclientconnection = false;
    double global_lat, global_lon;
    String response, phone_no;
    final static int  MY_PERMISSIONS_REQUEST_CONTACTS=2;
    HashMap<String, String> mapping;
    Marker mymarker;

    Handler mHandler = new Handler();
    Runnable mTask = new Runnable() {
        public void run() {
            System.out.println("..................locating........................");
            friend_getter();
        }
    };
    public MainActivityFragment() {
        // Required empty public constructor
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(LOCATION_KEY, mLocation);
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequestingLocationUpdates = false;
        mapping=new HashMap<>();
        getContacts();
        phone_no = this.getArguments().getString(Constants.PHONENUMBER);
        updateValuesFromBundle(savedInstanceState);
        buildGoogleApiClient();
        mGoogleApiClient.connect();
        if (getArguments() != null) {

        }



    }

    public void friend_getter() {
        /* Thread which refreshes the map showing friends every 5 seconds */

        HashMap<String,String> postdataparams=new HashMap<>();


                /* TODO: Get location from arguments */
        postdataparams.put("fetch_friends", "on");
        postdataparams.put("px",Double.toString(global_lat));
        postdataparams.put("py",Double.toString(global_lon));
        postdataparams.put("self_id", phone_no);
        Communicate c = new Communicate(postdataparams);

        response = c.performPostCall();

        if(mMap!=null){
            mMap.clear();}


        locating(response);
        //TODO: write the logic for handling the response from the server.
        mHandler.postDelayed(mTask, 5000);
    }

    public void locating(String response)
    {
        ArrayList<String> nearby_friends = new ArrayList<>();
        ArrayList<Double> latitude = new ArrayList<>();
        ArrayList<Double> longitude = new ArrayList<>();



        if(response!=null){

            //write the logic to get infdividual numbers and lat long positions
            //assume it would be hashmap of numbers and lat long positions
            try {
                JSONObject obj = new JSONObject(response);
                JSONArray result = obj.getJSONArray("result");
                for (int i=0; i < result.length(); i++) {
                    JSONObject inner_obj = result.getJSONObject(i);
                    nearby_friends.add(inner_obj.getString("friend"));

                    latitude.add(Double.parseDouble(inner_obj.getString("lat")));
                    longitude.add(Double.parseDouble(inner_obj.getString("lon")));
                }
                LatLng loc =new LatLng(global_lat, global_lon);
                MarkerOptions mp = new MarkerOptions();
                mp.position(loc);
                mp.title("Me");

             Marker my =   mMap.addMarker(mp);
                my.showInfoWindow();
                if (longitude.size() == 0) {

                }
                else {

                    for(int i=0;i<longitude.size();i++) {
                        String title=mapping.get(nearby_friends.get(i));

                        loc =new LatLng(latitude.get(i), longitude.get(i));
                        mp = new MarkerOptions();
                        mp.position(loc);
                        mp.title(title);

                        mMap.addMarker(mp).showInfoWindow();



                    }

                }
            }


            catch (JSONException e) {
                Log.e("MYAPP", "unexpected JSON exception", e);
                // Do something to recover ... or kill the app.
            }


        }


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_main_activity, container, false);
        mapView = (MapView) root.findViewById(R.id.mapview1);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        //setUpMapIfNeeded(root);
        try {
            MapsInitializer.initialize(getContext());
        } catch (Exception e) {
            // TODO handle this situation
        }
         root.setClickable(true);

        return root;
    }

    private void setUpMapIfNeeded(View inflatedView) {
        if (mMap == null) {
            ((MapView) inflatedView.findViewById(R.id.map)).getMapAsync(this);
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    public void onMapReady(GoogleMap googleMap) {

                    mMap = googleMap;
                    UiSettings usettings = mMap.getUiSettings();
                    usettings.setZoomControlsEnabled(true);
                    usettings.setCompassEnabled(true);
                    usettings.setMyLocationButtonEnabled(true);
                    mMap.setOnMyLocationButtonClickListener(this);
                    mMap.setOnMapClickListener(this);
                    enableMyLocation();
                    setUpMap();
                    mTask.run();
                }


    private void enableMyLocation() {
                if (!mPermissionDenied) {
                    // Permission to access the location is missin
                    //PermissionUtils.requestPermission(getActivity(), LOCATION_PERMISSION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION, true);
                } else if (mMap != null) {
                    if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            // Toast.makeText(this, "needs the permission so that your acquaintances can see you ", Toast.LENGTH_LONG).show();

                        } else {
                            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                        }
                    }
                    // Access to the location has been granted to the app.
                    mMap.setMyLocationEnabled(true);
                }
            }

            @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
                switch (requestCode) {
                    case MY_PERMISSIONS_REQUEST_LOCATION: {
                        // If request is cancelled, the result arrays are empty.
                        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            // permission was granted, yay! Do the contacts-related task you need to do.
                        } else {
                            // permission denied, boo! Disable the functionality that depends on this permission
                        }

                return;
            }
                    case MY_PERMISSIONS_REQUEST_CONTACTS:{
                        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                            // permission was granted, yay! Do the contacts-related task you need to do.
                        }
                        else {
                            // permission denied, boo! Disable the functionality that depends on this permission
                        }
                        return;

                    }
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext()).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();

        createLocationRequest();
    }

    public void onConnected(Bundle connectionHint) {

        Gclientconnection = true;
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Toast.makeText(this, "needs the permission so that your acquaintances can see you ", Toast.LENGTH_LONG).show();

            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
        if (mLocation == null) {
            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            MarkerOptions mp = new MarkerOptions();
            if (mLocation != null) {
                global_lat = mLocation.getLatitude();
                global_lon = mLocation.getLongitude();
                //mymarker = mMap.addMarker(mp);
                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLocation.getLatitude(), mLocation.getLongitude()), 16));

            }
            else {
                mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            }

        } else {
            startLocationUpdates();
        }
  setUpMap();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Gclientconnection = false;

    }


    @Override
    public void onConnectionSuspended(int cause) {


        mGoogleApiClient.connect();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        checking();
    }

    protected void checking() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates s = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED: {
                        // Location settings are not satisfied, but this can be fixed  // by showing the user a dialog.
                        /*try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(OuterClass.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                           e.printStackTrace();
                        }*/
                        break;
                    }
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }
        });
    }


    protected void startLocationUpdates() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Toast.makeText(this, "needs the permission so that your acquaintances can see you ", Toast.LENGTH_LONG).show();

            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    public void onLocationChanged(Location location) {
        mLocation = location;
        global_lat = location.getLatitude();
        global_lon = location.getLongitude();
        String arg[] = {Double.toString(location.getLatitude()), Double.toString(location.getLongitude())};
        mymarker.remove();
        MarkerOptions mp = new MarkerOptions();
        mp.position(new LatLng(location.getLatitude(), location.getLongitude()));
        mp.title("my position");
        mymarker= mMap.addMarker(mp);
        mymarker.showInfoWindow();
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 16));
        HashMap <String,String> postdataparams = new HashMap<>();


                /* TODO: Get location from arguments */

        postdataparams.put("px",Double.toString(global_lat));
        postdataparams.put("py",Double.toString(global_lon));
        postdataparams.put("self_id", phone_no);
        Communicate c = new Communicate(postdataparams);
        response = c.performPostCall();
    }


    public void onMapClick(LatLng point) {

        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(point, 16);
        mMap.animateCamera(update);

    }

    private void setUpMap() {

        b=mGoogleApiClient.isConnected();

        if (b) {

            mMap.addMarker(new MarkerOptions().position(new LatLng(global_lat, global_lon)).title("Me")).showInfoWindow();
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(global_lat,global_lon), 12);
            mMap.animateCamera(update);
        } else {
            mGoogleApiClient.connect();
        }
    }


    @Override
    public void onDestroy() {

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        mapView.onDestroy();
        super.onDestroy();
    }

    public boolean onMyLocationButtonClick() {
        Toast.makeText(getContext(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        Location loc = mLocation;
        LatLng ll = new LatLng(loc.getLatitude(), loc.getLongitude());
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 20);
        mMap.animateCamera(update);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

    }
    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
        mapView.onPause();

    }

    @Override
    public void onStop() {

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        //Log.i(TAG, "Updating values from bundle");
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(REQUESTING_LOCATION_UPDATES_KEY);
            }
            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                // Since LOCATION_KEY was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mLocation = savedInstanceState.getParcelable(LOCATION_KEY);
            }


        }
    }
    public void getContacts() {

        ArrayList<String> contacts = new ArrayList<>();
        ContentResolver cr = getContext(). getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        try {
                            String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                            String str = phoneNo.replaceAll("[^0-9]", "");
                            str = str.replaceAll("\\s+", "");
                            str = str.substring(str.length() - 10);

                            contacts.add(str);
                            mapping.put(str, name);
                            break;
                        }
                        catch (RuntimeException e) {
                            e.printStackTrace();
                        }


                        //  Toast.makeText(NativeContentProvider.this, "Name: " + name + ", Phone No: " + phoneNo, Toast.LENGTH_SHORT).show();
                    }
                    pCur.close();
                }
            }

        }
        cur.close();
    }
}