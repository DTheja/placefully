package com.example.ideaquest.placefully;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.SupportMapFragment;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import android.support.v4.app.Fragment;
public class Notification2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification2);


            Intent intent;
            intent = getIntent();
            //String message = intent.getStringExtra(Constants.NOTIFICATIONRESPONSE);
            String self_id = intent.getStringExtra("SELF_ID");
            String latitude = intent.getStringExtra("LATITUDE");
            String longitude = intent.getStringExtra("LONGITUDE");


        System.out.println("............latitude"+latitude+"..........");
        System.out.println("...............longitude"+longitude+"......................");
            Geocoder geocoder;
            Double lat, lon;
            lat = Double.parseDouble(latitude);
            lon = Double.parseDouble(longitude);

            geocoder = new Geocoder(this, Locale.getDefault());
            String faddress = "";
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lon, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
              //  String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
                //faddress = knownName+"\n"+address+city+"\n"+state+"\n"+country;
                faddress = knownName+"\n"+city+"\n"+state+"\n"+country;

            } catch (IOException e) {
                e.printStackTrace();
            }
        catch(IndexOutOfBoundsException t){t.printStackTrace();}
            /* We can discuss about this.
            Intent i =new Intent();
            i.putExtra(Constants.FULL_ADDRESS,"faddress");
            // no need to create an artificial back stack.
            PendingIntent resultPendingIntent =
                    PendingIntent.getActivity(
                            getContext(),
                            0,
                            i,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
            */

            Bundle b= new Bundle();
            b.putString(Constants.FULL_ADDRESS, faddress);
            b.putString(Constants.HELP_PERSON, self_id);
            b.putDouble("Latitude",lat);
            b.putDouble("Longitude",lon);
        if (findViewById(R.id.fragment_container2) != null) {
            NotificationMessage nm=new NotificationMessage();
            nm.setArguments(getIntent().getExtras());
            nm.setArguments(b);
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container2,nm).addToBackStack(null).commit();

        }
//            NotificationMessage nm=new NotificationMessage();
//            nm.setArguments(b);
//            displayFragment(nm,"Notification Message");


        /* TODO: Move to the fragment NotificationMessage after this, and bundle the address
        * Also, what the hell is this ^ ?*/
    }


//    private void displayFragment(Fragment fragment, String title){
//        if (fragment != null){
//             fragmentManager = getFragmentManager();
//            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.fragment_container, fragment);
//            //fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();
//            // Set Toolbar Title
//        }
//    }
}
