package com.example.ideaquest.placefully;

/**
 * Created by DIVYATHE on 7/8/2016.
 */
public class Constants {
    public static final String MAPPING="mapping contacts";
    public static final String CONTACTS="contacts";
    public static final String NAMES="names";
    public static final String REVIEW_POINT="review point";
    public static final String REVIEW_STRING="review string";
    public static final String PHONENUMBER="phonenumber";
    public static final String FULL_ADDRESS="faddress";
    public static final String SELF_ID="self_id";
    public static final String LATITUDE ="latitude";
    public static final String LONGITUDE="longitude";
    public static final String NOTIFICATIONRESPONSE="response";
    public static final String HELP_PERSON = "help_person";


}
